FROM ubuntu:22.04

COPY install-packages.sh .
RUN ["chmod", "+x", "./install-packages.sh"]
RUN ./install-packages.sh
